Hero: Podlibre
HeroSubtitle: Podlibre est une plateforme d'hébergement de podcasts orientés principalement vers le logiciel libre.
Title: La plateforme

Podlibre est une plateforme qui héberge des podcasts audio orientés
principalement vers le logiciel libre, le système d'exploitation Linux et l'Open
Source en général. Nous hébergeons actuellement <em>[Parole de
Tux!](/paroledetux)</em>, un podcast belge qui prit forme entre 2012 et 2014.

D'autres projets de podcasts sont actuellement en travaux.

---

<div class="columns has-text-centered">
  <div class="column">
    <div class="card" style="margin:auto;">
      <div class="card-image">
        <figure class="image is-4by3">
          <img src="/images/paroledetux.png" alt="Parole de Tux">
        </figure>
      </div>
      <div class="card-content">
        <h3 class="title is-4">Parole de Tux!</h3>
        <p>Parole de Tux! est un podcast qui vous parle de GNU/Linux, de l'open source, du logiciel libre, des jeux sous Linux, de sujets geeks...</p>
      </div>
      <footer class="card-footer">
          <a class="card-footer-item" href="/paroledetux/">Blog</a>
          <a class="card-footer-item" href="/feeds/podcast/paroledetux.rss.xml">RSS</a>
      </footer>
    </div>
  </div>
  <div class="column">
    <div class="card" style="margin:auto;">
      <div class="card-image">
        <figure class="image is-4by3">
          <img src="/images/gpl.png" alt="Le Grand Podcast Libre">
        </figure>
      </div>
      <div class="card-content">
        <h3 class="title is-4">Le Grand Podcast Libre</h3>
        <p>Le Grand Podcast Libre (GPL) est un podcast qui traite du logiciel libre et de l'Open Source en général.</p>
      </div>
      <footer class="card-footer">
        <div class="card-footer-item">
          Décembre 2016
        </div>
      </footer>
    </div>
  </div>
</div">
