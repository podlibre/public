Title: Merci, Fosdem et Futur
Date: 13-01-2015
Author: CaptainPosix

Salut les manchots,

Ça fait une paye hein... Bon, d'abord merci à tous pour les gentils commentaires,
pour les mails que j'ai reçu et toute cette manifestation de votre part qui nous
laisse à penser qu'on n'a pas fait ça pour rien.
Les commentaires

Je veux juste revenir sur deux ou trois commentaires, comme je l'aurais fait
dans le podcast. Je ne peux pas revenir sur tous parce que vous avez été
nombreux, mais ça nous a fait très très chaud au coeur dans ce froid (très
froid) début d'année 2015.

Koun a dit:

> 'tain, j'ai l'impression de perdre de vue des potes...

Ouais en effet... Après rapide auto-analyse de l'arrêt du podcast, c'est pas
tant le podcast qui va me manquer, mais l'échange qu'on a pu avoir avec vous
tous.

BlogueLinux.ca a dit:

> Peut-être aurons nous la chance de collaborer ensemble au cours de 2015

Oui, ça reste dans mes intentions. Je dois juste trouver comment compiler ça
avec les nouvelles activités. Je vous tiens tous les trois informés.

Maxeric a dit:

> Votre amateurisme professionnel, qui faisait un peu de vous des ovnis va me
manquer !

Alors ça c'est le truc qui me séduit aussi à fond. Finalement quand on commence
le podcast, on se demande "au nom de qui et de quoi on se permet de croire qu'on
connait mieux que d'autres personnes certains sujets, quelle est notre
légitimité...??". Puis on avance et on prend de l'assurance sur la forme et on
commence a assumer le fond (on ne l'améliore pas...on l'assume :-)).

Kelevra a dit

> Vous ferez quand même un remerciement pour les commentaires ? ^^

Oui, c'est ce que je fais :-)

Chris a dit

> Comme le dit le renard au Petit Prince de Saint-Exupéry :
> > J'y gagne, dit le renard, à cause de la couleur du blé.

Bon alors ça c'est juste trop comme référence par rapport à ce qu'on a fait,
c'est démesuré que ce soit au niveau de la référence (le Petit Prince) ou au
niveau de sa signification..., mais c'est un des commentaires qui m'a fait assez
violemment pincé le coeur... Je suis allé relire ce chapitre avec le renard et
récemment je l'ai expliqué à ma fille...

Lionel_c a dit

> A vendre un dossard pour le Marathon de Paris 2015
> Non, ce n'est pas une connerie.
> Je revends mon dossard puisque l'épisode d'avril 2015 de votre podcast
> n'existera pas et que c'était mon dopage à moi :-(

Bon, encore un commentaire exagéré...C'est un peu trop pour nous ça... Ceci dit,
Parole De Tux est terminé mais la relève est assurée. Je te souhaite de passer
sous les 3h10 pour ton marathon de Paris... Et j'essayerai que "la relève" tire
en longueur ;-)

Bon donc encore un grand merci à vous.

## Le FOSDEM

Ben voilà, on va essayer d'y aller, au moins Bubulle et moi. Tolrek n'est pas
encore trop décidé. Je ne pense pas aller à des conférences, j'y vais juste pour
rencore Donkluivert et Arpinux, donc toute personnes qui passe dans le coin et
qui souhaite partager avec nous une bonne bière belge peut m'envoyer un mail
(captain.posix@gmail.com).

Vu que Donkluivert vient seulement samedi matin, c'est certainement le moment où
on y sera... Probablement avec la tête un peu enfoncée puisqu'il y a une soirée
qui se profile la veille pour nous deux...Mais on fera les forts.

https://fosdem.org/2015/

## Le futur

Le futur c'est le nouveau podcast de Mika. Je suis très heureux de participer à
cette aventure. Il sera prochainement en ligne donc je vous mettrai un lien dés
qu'il sera sorti. Je dois encore composer le générique donc comme à mon
habitude, ça prendra entre deux et trois mois.

Bise à vous, portez vous bien.
