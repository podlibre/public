Title: Parole de Tux
Template: podcast-landing

<div class="notification is-success">Le podcast <em>Parole de Tux!</em>
a pris fin en décembre 2014 et n'est actuellement plus produit.</div>

Parole de Tux, c'est Captain Posix, Bubulle et Tolrek qui vous parlent de
GNU/Linux, de l'open source, du logiciel libre, des jeux sous Linux, de sujets
geeks et quand y'en a un peu plus, on vous le met aussi...
