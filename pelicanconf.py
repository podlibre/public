#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Julien Pivotto'
SITENAME = u'Podlibre'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'
LOCALE = u'fr_FR.utf-8'

# Feed generation is usually not desired when developing
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
PODCAST_FEED_ALL_RSS = 'feeds/podcast/all.rss.xml'
PODCAST_FEED_RSS = 'feeds/podcast/%s.rss.xml'


import sys
sys.path.append('./')

from plugins import add_directory_metadata, podcast_medadata, podcast_feed
PLUGINS = [add_directory_metadata, podcast_medadata, podcast_feed]


DEFAULT_PAGINATION = 10


SLUGIFY_SOURCE='basename'

RELATIVE_URLS = True

THEME='./theme'
DIRECT_TEMPLATES = []

LOAD_CONTENT_CACHE = False
USE_FOLDER_AS_CATEGORY = False

DEFAULT_METADATA = {
    'translation': 'true',
}

DEFAULT_DATE_FORMAT = '%d %B %Y'

SUMMARY_MAX_LENGTH = 10

from pelicandata import *
