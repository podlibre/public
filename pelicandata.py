from __future__ import unicode_literals
import os
DEFAULT_CATEGORY = 'podlibre'

LINKS = (('Bloguelinux.ca', 'http://bloguelinux.ca/'),)

MENUITEMS = (('Code Source', 'https://framagit.org/podlibre/public'),)

PODCASTS = (
    ('Parole de Tux!', 'paroledetux'),
#    ('Le Grand Podcast Libre', 'gpl'),
)

TABS = (
    ('La plateforme', '', 'podlibre'),
#    ('Le Grand Podcast Libre', 'gpl/', 'gpl'),
    ('Parole de Tux!', 'paroledetux/', 'paroledetux'),
)

PODCAST_ARTICLE_DIRECTORY='blog'
PODCAST_EPISODE_DIRECTORY='podcast'

SOCIAL = ()

ARTICLE_PATHS = []
for _, slug in PODCASTS:
    ARTICLE_PATHS.append(os.path.join(slug, PODCAST_ARTICLE_DIRECTORY))
    ARTICLE_PATHS.append(os.path.join(slug, PODCAST_EPISODE_DIRECTORY))

PAGE_PATHS = ['']

PAGE_SAVE_AS='{directory}{_slug}.html'
ARTICLE_URL='{directory}{_slug}.html'

ARTICLE_SAVE_AS='{directory}{_slug}.html'
ARTICLE_URL='{directory}{_slug}.html'

CATEGORY_METADATA = {
    'paroledetux': {
        'hero': 'Parole de Tux!',
        'herosubtitle': 'Parole de Tux! est un <strong>podcast</strong> qui vous parle de GNU/linux, de l\'open source, du logiciel libre, des jeux sous Linux, de sujets geeks...',
    }
}

STATIC_PATHS = [
    'images',
]
