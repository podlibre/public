import os.path
from pelican import signals

def add_directory_metadata(content):
    base_path = content.settings['PATH']
    source_dir = os.path.dirname(content.source_path)
    relative_path = os.path.relpath(source_dir, base_path)
    if relative_path != '.':
        content.metadata['directory'] = os.path.join(relative_path, '')
    else:
        content.metadata['directory'] = ''

def register():
    signals.content_object_init.connect(add_directory_metadata)

