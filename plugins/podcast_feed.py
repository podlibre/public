from pelican import signals
import logging
from operator import attrgetter
from feedgen.feed import FeedGenerator
import os.path

def generate_feed(article_generator, writer, title, category, path, link, alt_link, articles):
    site_url = article_generator.settings.get('SITEURL')
    fg = FeedGenerator()
    fg.id( link)
    fg.title( title)
    fg.description('Podcast')
    fg.link( href=alt_link, rel='alternate' )
    fg.link( href=link, rel='self' )
    fg.language('fr')
    logging.info('Writing feed to %s' % path)

    fg.load_extension('podcast')
    #fg.podcast.itunes_category('Technology', 'Podcasting')

    for article in articles:
        if not article.episode:
            continue
        fe = fg.add_entry()
        url = '%s/files/%s/%s' % (site_url, article.podcast, article.file)
        fe.id(url)
        fe.title(article.title)
        fe.description('%s' % article.summary)
        fe.enclosure(url, article.filesize, 'audio/ogg')

    fg.rss_file(path)


def podcast_feed(article_generator, writer):
        feed_domain = article_generator.settings.get('FEED_DOMAIN')
        sitename = article_generator.settings.get('SITENAME')

        if article_generator.settings.get('PODCAST_FEED_ALL_RSS'):
            all_articles = list(article_generator.articles)
            all_articles.sort(key=attrgetter('date'), reverse=True)

            if article_generator.settings.get('PODCAST_FEED_ALL_RSS'):
                articles = []
                for article in all_articles:
                    if article.episode:
                        articles.append(article)
                rss = article_generator.settings.get('PODCAST_FEED_ALL_RSS')
                complete_path = os.path.join(article_generator.output_path, rss)
                try:
                    os.makedirs(os.path.dirname(complete_path))
                except Exception:
                    pass
                generate_feed(article_generator, writer, '%s - Tous les podcasts' % sitename,
                              None, complete_path, '%s/%s' % (
                              feed_domain, rss), feed_domain, articles)

        if article_generator.settings.get('PODCAST_FEED_RSS'):
            all_articles = list(article_generator.articles)
            all_articles.sort(key=attrgetter('date'), reverse=True)

            if article_generator.settings.get('PODCAST_FEED_RSS'):
                for podcastname, podcast in article_generator.settings.get('PODCASTS'):
                    articles = []
                    for article in all_articles:
                        if article.episode and article.podcast == podcast:
                            articles.append(article)

                    rss = article_generator.settings.get('PODCAST_FEED_RSS') % (podcast)
                    complete_path = os.path.join(article_generator.output_path, rss)
                    try:
                        os.makedirs(os.path.dirname(complete_path))
                    except Exception:
                        pass
                    generate_feed(article_generator, writer, '%s' % podcastname,
                                None, complete_path, '%s/%s' % (
                                feed_domain, rss), feed_domain, articles)

def register():
    signals.article_writer_finalized.connect(podcast_feed)

