import os.path
import logging
from pelican import signals
from pelican.contents import Category
logging.basicConfig(level=logging.INFO)

def podcast_metadata(content):
    podcasts = dict(content.settings['PODCASTS']).values()
    episode_suffix = content.settings['PODCAST_EPISODE_DIRECTORY']
    article_suffix = content.settings['PODCAST_ARTICLE_DIRECTORY']
    base_path = content.settings['PATH']
    source_dir = os.path.dirname(content.source_path)
    relative_path = os.path.relpath(source_dir, base_path)
    for podcast in podcasts:
        podcast_episode_prefix = os.path.join(podcast, episode_suffix)
        podcast_article_prefix = os.path.join(podcast, article_suffix)
        if relative_path == podcast_episode_prefix:
            logging.warning('%s is an episode of %s' % (content.source_path, podcast))
            content.metadata['podcast'] = podcast
            content.metadata['episode'] = True
            break
        elif relative_path == podcast_article_prefix:
            logging.info('%s is an article of %s' % (content.source_path, podcast))
            content.metadata['podcast'] = podcast
            content.metadata['episode'] = False
            break
        elif relative_path.startswith(podcast):
            content.metadata['podcast'] = podcast
            content.metadata['episode'] = False

    if 'episode' not in content.metadata:
        logging.info('%s is an article out of any podcast' % content.source_path)
        content.metadata['podcast'] = None
        content.metadata['episode'] = False

    content.metadata['_slug'] = content.slug

    if 'podcast' in content.metadata and content.metadata['podcast']:
        content.metadata['category'] = content.metadata['podcast']
        content.slug = '%s/%s' % (content.metadata['podcast'], content.slug)
        content.category = Category(content.metadata['category'], content.settings)

    if 'category' in content.metadata and content.metadata['category']:
        category = content.metadata['category']
        settings = content.settings['CATEGORY_METADATA']
        if category in settings:
            for k, v in settings[category].items():
                if k not in content.metadata and not getattr(content, k, None):
                    logging.info('Setting %s as %s' % (k, v))
                    content.metadata[k] = v
                    setattr(content, k, v)
    setattr(content, 'podcast', content.metadata['podcast'])
    setattr(content, 'episode', content.metadata['episode'])

def register():
    signals.content_object_init.connect(podcast_metadata)

